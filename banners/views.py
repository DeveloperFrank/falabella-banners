from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

def home(request):
	return render(request, 'home.html')

def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user = form.save()
			return redirect('home')
	else:
		form = UserCreationForm()
	return render(request, 'registration/signup.html', {
		'form': form
	})

def login(request):
	return render(request, 'registration/login.html')

@login_required
def main(request):
	return render(request, 'main.html')

@login_required
def tiendas(request):
	return render(request, 'tiendas.html')

@login_required
def price(request):
	return render(request, 'banner_precio_producto.html')

@login_required
def percent(request):
	return render(request, 'banner_porcentaje_producto.html')

