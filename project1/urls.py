from django.contrib import admin
from django.urls import path, include

from banners import views

urlpatterns = [
    path('', views.home, name='home'),
    path('signup/', views.signup, name="signup"),
    path('login/', views.login, name="login"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    path('main/', views.main, name='main'),
    path('tiendas/', views.tiendas, name='tiendas'),
    path('producto-precio/', views.price, name='producto-precio'),
    path('producto-porcentaje/', views.percent, name='producto-porcentaje')
]
